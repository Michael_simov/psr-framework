<?php

use Zend\HttpHandlerRunner\Emitter\SapiEmitter;
use Zend\Diactoros\ServerRequestFactory;

chdir(dirname(__DIR__));
require 'vendor/autoload.php';


### Init ###

$params = [
    'users' => [ 'admin' => 'admin' ]
];

$aura = new \Aura\Router\RouterContainer();
$routes = $aura->getMap();

$routes->get('home', '/', \App\Http\Actions\HomeAction::class);
$routes->get('cabinet', '/cabinet', [
    new \Framework\Http\Middleware\BasicAuthMiddleware($params['users']),
    \App\Http\Actions\CabinetAction::class
]);

$router = new \Framework\Http\Router\AuraRouterAdapter($aura);
$resolver = new \Framework\Http\MiddlewareResolver();
$request = ServerRequestFactory::fromGlobals();

try
{
    $result = $router->match($request);
    foreach ($result->getAttributes() as $attribute => $value) {
        $request = $request->withAttribute($attribute, $value);
    }

    $handlers = $result->getHandler();
    $pipeline = new \Framework\Http\Pipeline\Pipeline();
    foreach (is_array($handlers) ? $handlers : [$handlers] as $handler) {
        $pipeline->pipe($resolver->resolve($result->getHandler()));
    }
    $response = $pipeline($request, new \Framework\Http\Middleware\NotFoundMiddleware());
} catch (\Framework\Http\Router\Exceptions\RequestNotMatchExeption $e) {
    $response = (new \Framework\Http\Middleware\NotFoundMiddleware())($request);
}

### Postproc ###

$response = $response->withHeader('X-Dev', 'michael');

$emitter = new SapiEmitter();
$emitter->emit($response);