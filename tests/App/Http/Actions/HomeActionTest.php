<?php

namespace Tests\App\Http\Actions;

use App\Http\Actions\HomeAction;
use PHPUnit\Framework\TestCase;
use Zend\Diactoros\ServerRequest;

class HomeActionTest extends TestCase
{
    public function testHome()
    {
        $action = new HomeAction();
        $request = new ServerRequest();

        $response = $action($request);

        self::assertEquals(200, $response->getStatusCode());
        self::assertJson('{"title":"Home page"}', $response->getBody()->getContents());
    }
}