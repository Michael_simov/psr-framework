<?php

namespace App\Http\Actions;

use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class CabinetAction
{
    public function __invoke(ServerRequestInterface $request)
    {
        return new JsonResponse(['title' => 'Cabinet page']);
    }
}