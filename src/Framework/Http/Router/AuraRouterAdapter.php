<?php

namespace Framework\Http\Router;


use Aura\Router\Exception\RouteNotFound;
use Aura\Router\RouterContainer;
use Framework\Http\Router\Exceptions\RequestNotMatchExeption;
use Framework\Http\Router\Exceptions\RouteNotFoundException;
use Psr\Http\Message\ServerRequestInterface;

class AuraRouterAdapter implements Router
{
    private $aura;

    public function __construct(RouterContainer $container)
    {
        $this->aura = $container;
    }

    public function match(ServerRequestInterface $request)
    {
        $matcher = $this->aura->getMatcher();
        if($route = $matcher->match($request)) {
            return new Result($route->name, $route->handler, $route->attributes);
        }
        throw new RequestNotMatchExeption($request);
    }

    public function generate($name, array $params): string
    {
        $generator = $this->aura->getGenerator();
        try{
            return $generator->generate($name, $params);
        } catch (RouteNotFound $e) {
            throw new RouteNotFoundException($name, $params, $e);
        }
    }
}