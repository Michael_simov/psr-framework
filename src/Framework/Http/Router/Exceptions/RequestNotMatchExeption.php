<?php

namespace Framework\Http\Router\Exceptions;

use Psr\Http\Message\ServerRequestInterface;

class RequestNotMatchExeption extends \LogicException
{

    private $request;

    public function __construct(ServerRequestInterface $request)
    {
        parent::__construct('Matches not found');
        $this->request = $request;
    }

    /**
     * @return ServerRequestInterface
     */
    public function getRequest(): ServerRequestInterface
    {
        return $this->request;
    }
}