<?php

namespace Framework\Http\Middleware;

use Psr\Http\Message\ServerRequestInterface;

class NotFoundMiddleware
{
    public function __invoke(ServerRequestInterface $request)
    {
        return new \Zend\Diactoros\Response\HtmlResponse('Page Not Found', 404);
    }
}