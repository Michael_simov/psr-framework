<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.01.19
 * Time: 13:47
 */

namespace Framework\Http\Pipeline;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Request;

class Pipeline
{
    private $queue;

    public function __construct()
    {
        $this->queue = new \SplQueue();
    }

    public function pipe(callable $middleware): void
    {
        $this->queue->enqueue($middleware);
    }

    public function __invoke(ServerRequestInterface $request, callable $default): ResponseInterface
    {
        return $this->next($request, $default);
    }

    public function next(ServerRequestInterface $request, callable $default)
    {
        $delegate = new Next($this->queue, $default);

        return $delegate($request);
    }
}